import React, { Component } from 'react';

export default class ListItem extends Component {

    render() {
        return (
            <div className="row">
                <div className="col-9">
                    <h4>Name</h4>
                    <p>description</p>
                </div>
                <div className="col-3">
                    <div className="row">
                        <div className="col-6">
                            remove
                        </div>
                        <div className="col-6">
                            edit
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}