import React, { Component } from 'react';
import './App.css';
import List from '../List'

export default class App extends Component {
 
  render() {
    return (
      <div className="container">
        <header>
          <h2>Hotdogs</h2>
        </header>  
        <main>
          <List />
        </main>
        <footer>
        </footer>
      </div>
    )
  }
}