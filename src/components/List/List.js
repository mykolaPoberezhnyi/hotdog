import React, { Component } from 'react';
import ListItem from '../ListItem'

export default class List extends Component {

    constructor() {
        super();

        this.state = {
            hotdogs: [
                {
                    id: 1,
                    name: "Name 1",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 2,
                    name: "Name 2",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 3,
                    name: "Name 3",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 4,
                    name: "Name 4",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 5,
                    name: "Name 5",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 6,
                    name: "Name 6",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                },
                {
                    id: 7,
                    name: "Name 7",
                    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
                }
            ]
        }
    }
 
    render() {
        return (
            <div>
                <ListItem />
            </div>
        )
    }
}